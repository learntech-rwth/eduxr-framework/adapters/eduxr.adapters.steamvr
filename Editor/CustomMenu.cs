﻿using OmiLAXR.Utils;
using UnityEditor;

namespace OmiLAXR.Adapters.UnityXR
{
    internal class CustomMenu
    {
        [MenuItem("GameObject / OmiLAXR / Adapters / SteamVR Adapter")]
        private static void AddSteamVRAdapter()
            => EditorUtils.AddPrefabToSelection("Prefabs/SteamVR_Adapter");
    }
}
