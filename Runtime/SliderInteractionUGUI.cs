﻿using System;
using OmiLAXR.Interaction;
using UnityEngine;
using UnityEngine.UI;

namespace OmiLAXR.UI
{

    /// <summary>
    /// Similar to SliderInteraction, but wraps a underlying UGUI Slider.
    /// </summary>
    [RequireComponent(typeof(Pointable), typeof(Slider))]
    [Obsolete("Check if it is still needed. Delete if not.")]
    public class SliderInteractionUGUI : MonoBehaviour
    {
        private Slider slider;
        private RectTransform rect;
        private Pointable pointable;
        private bool isHeld = false;

        private Vector3 hit;

        private void Start()
        {
            slider = GetComponent<Slider>();

            pointable = GetComponent<Pointable>();
            pointable.onSelect.AddListener(_ => isHeld = true);
            pointable.onUnselect.AddListener(_ => isHeld = false);

            rect = GetComponent<RectTransform>();

            LaserPointerHandler.Instance.OnHit += hitPoint => hit = hitPoint.point;
        }

        private void Update()
        {
            if (isHeld)
            {
                float local = rect.worldToLocalMatrix.MultiplyPoint(hit).x;

                // normalize to [0, 1] over the horizontal range of the slider
                local = (local / rect.rect.width) + rect.pivot.x;

                slider.value = local;
            }
        }
    }
}