﻿using Valve.VR;
using UnityEngine;

namespace OmiLAXR.Adapters.SteamVR
{
    public static class SteamVR_RenderModel_Ext
    {
        private const string BODY = "body";
        private const string TRACKPAD_TOUCH = "trackpad_touch";
        private const string TRACKPAD = "trackpad";
        public static GameObject GetBody(this SteamVR_RenderModel renderModel) => renderModel.transform.Find(BODY).gameObject;

        public static GameObject GetTrackpadTouchAttach(this SteamVR_RenderModel renderModel)
        {
            var trackpadTouch = renderModel.transform.Find(TRACKPAD_TOUCH);
            return trackpadTouch.Find("attach").gameObject;
        }

        public static void AddControllerBodyCollider(this SteamVR_RenderModel renderModel)
        {
            if (!renderModel)
                return;

            var body = renderModel.transform.Find(BODY);

            if (body.GetComponent<MeshCollider>() == null)
                body.gameObject.AddComponent<MeshCollider>();

            // Skip if a body exists
            if (body.gameObject.GetComponent<Rigidbody>() != null)
                return;

            var bodyRB = body.gameObject.AddComponent<Rigidbody>();

            if (bodyRB == null)
                return;

            bodyRB.isKinematic = true;
            bodyRB.useGravity = false;
        }

        public static void PlaceOnTrackpad(this SteamVR_RenderModel renderModel, GameObject gameObject)
        {
            var trackpad = renderModel.transform.Find(TRACKPAD);

            // skip if cannot find trackpad
            if (trackpad == null)
                return;

            var attach = trackpad.Find("attach");

            var t = gameObject.transform;
            // place object on trackpad
            t.parent = attach.transform;
            t.localPosition = Vector3.zero;
            t.localEulerAngles = Vector3.zero;
        }
    }

}