﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR.InteractionSystem;
using OmiLAXR.Interaction;
using Valve.VR;

namespace OmiLAXR.Adapters.SteamVR
{
    public class VRPlayer : IPlayer
    {
        private readonly string[] _actions = GetAllActionsNames();
        private readonly Player _vivePlayer;
        private readonly GameObject _leftHand;
        private readonly GameObject _rightHand;
        private readonly SteamVR_ImprovedLaserPointer _leftLaser;
        private readonly SteamVR_ImprovedLaserPointer _rightLaser;
        private readonly Camera _camera;
        private readonly Transform _body;
        private readonly Transform _head;
        public VRPlayer(Player player)
        {
            _vivePlayer = player;

            var steamVrObjects = player.transform.Find("SteamVRObjects");
            
            _leftHand = steamVrObjects.Find("LeftHand").gameObject;
            _leftLaser = _leftHand.GetComponent<SteamVR_ImprovedLaserPointer>();
            _rightHand = steamVrObjects.Find("RightHand").gameObject;
            _rightLaser = _rightHand.GetComponent<SteamVR_ImprovedLaserPointer>();

            _camera = steamVrObjects.Find("VRCamera").GetComponent<Camera>();

            _body = steamVrObjects.Find("BodyCollider").transform;
            
            
            _head = player.transform.Find("FollowHead")?.transform;
            if (!_head)
            {
                _head = GameObject.Find("FollowHead").transform;
            }
            
            // Bind event to all tracking actions
            foreach (var action in SteamVR_Input.actionsBoolean)
            {
                action.AddOnChangeListener(OnStateChangeHandler, SteamVR_Input_Sources.LeftHand);
                action.AddOnChangeListener(OnStateChangeHandler, SteamVR_Input_Sources.RightHand);   
            }
        }
        
        public string[] GetControllerActions() => _actions;

        public Vector3 WorldPosition => GetTransform().position;
        public Vector3 WorldRotation => GetTransform().eulerAngles + _camera.transform.eulerAngles;

        /// <summary>
        /// Representing the transform of the player object.
        /// </summary>
        /// <returns>Transform of 'Player' instance of SteamVR Player Prefab.</returns>
        public Transform GetTransform() => _vivePlayer.transform;
        /// <summary>
        /// Representing the head of the player object.
        /// </summary>
        /// <returns>Transform of 'FollowHead' object of SteamVR Player Prefab.</returns>
        public Transform GetHead() => _head;
        /// <summary>
        /// Representing the body of the player object.
        /// </summary>
        /// <returns>Transform of 'BodyCollider' object of SteamVR Player prefab.</returns>
        public Transform GetBody() => _body;
        /// <summary>
        /// Representing the laser pointer representation of a non VR player.
        /// </summary>
        /// <returns>Reference to 'LaserPointer' of the left hand object of SteamVR Player prefab.</returns>
        public ILaserPointer GetLeftPointer() => _leftLaser;
        /// <summary>
        /// Representing the laser pointer representation of a non VR player.
        /// </summary>
        /// <returns>Reference to 'LaserPointer' of the right hand object of SteamVR Player prefab.</returns>
        public ILaserPointer GetRightPointer() => _rightLaser;
        /// <summary>
        /// Representing the left hand of a non VR Player.
        /// </summary>
        /// <returns>Reference to 'LeftHand' object of SteamVR Player prefab.</returns>
        public GameObject GetLeftHand() => _leftHand;
        /// <summary>
        /// Representing the left hand of a non VR Player.
        /// </summary>
        /// <returns>Reference to 'RightHand' object of SteamVR Player prefab.</returns>
        public GameObject GetRightHand() => _rightHand;
        /// <summary>
        /// Representing the camera of a non VR Player.
        /// </summary>
        /// <returns>Reference to the 'VRCamera' component of the 'FallbackObjects' object of SteamVR Player prefab.</returns>
        public Camera GetCamera() => _camera;

        public XRType XRType => XRType.VirtualReality;

        /// <summary>
        /// Representing a list of possible controller actions the player can do.
        /// </summary>
        /// <returns>Returns a list of controller actions.</returns>
        public event ActionChangedHandler OnActionChanged;
        /// <summary>
        /// Get all VR actions filtered by names.
        /// </summary>
        /// <param name="names">List of action names.</param>
        /// <returns>Filtered VR actions list.</returns>
        private static SteamVR_Action_Boolean[] GetActionsByNames(IEnumerable<string> names) => names
            .Select(name => SteamVR_Input.actionsBoolean.First(a => a.GetShortName() == name)).ToArray();
        /// <summary>
        /// Get all VR action names.
        /// </summary>
        /// <returns>List of names.</returns>
        public static string[] GetAllActionsNames() => SteamVR_Input.actionsBoolean.Select(a => a.GetShortName()).ToArray();
        
        private void OnStateChangeHandler(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (fromSource != SteamVR_Input_Sources.LeftHand && fromSource != SteamVR_Input_Sources.RightHand)
                return;
            
            var name = fromAction.ToString();
            var state = newState ? ActionState.Pressed : ActionState.Released;
            var side = fromSource == SteamVR_Input_Sources.LeftHand ? Learner.Body.Side.Left : Learner.Body.Side.Right;
            var gameObject = side == Learner.Body.Side.Left ? _leftHand : _rightHand;
            
            var hand = new Learner.Body.Hand(side, gameObject);
            OnActionChanged?.Invoke(this, hand, name, state);
        }
    }
}