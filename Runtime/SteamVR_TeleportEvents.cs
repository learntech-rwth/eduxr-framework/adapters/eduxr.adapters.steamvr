﻿using OmiLAXR.Interaction;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace OmiLAXR.Adapters.SteamVR
{
    /// <summary>
    /// Handler that fires events when teleporters are used.
    /// </summary>
    [RequireComponent(typeof(TeleportPoint))]
    public class SteamVR_TeleportEvents : TeleportEvents
    {
        private TeleportPoint _teleportPoint;
        private IPlayer _player;
        
        private void Awake()
        {
            _teleportPoint = GetComponent<TeleportPoint>();
            Teleport.Player.Listen(OnTeleport);
        }

        public override void FireTeleport()
        {
            var player = Learner.Instance.Player;
            if (Learner.Instance.IsDesktop)
            {
                var playerTransform = player.GetTransform(); 
                playerTransform.position = transform.position;
                OnVisit.Invoke();
                return;
            }
            _teleportPoint.TeleportPlayer(transform.position);
        }

        private void OnTeleport(TeleportMarkerBase teleportMarkerBase)
            => OnTeleport(teleportMarkerBase ? teleportMarkerBase.gameObject : null);
    }

}
