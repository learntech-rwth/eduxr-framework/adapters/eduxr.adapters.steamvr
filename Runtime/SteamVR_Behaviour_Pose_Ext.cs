﻿using Valve.VR;

namespace OmiLAXR.Adapters.SteamVR
{
    public static class SteamVR_Behaviour_Pose_Ext
    {
        public static int GetTrackedDeviceIndex(this SteamVR_Behaviour_Pose pose)
        {
            if (pose.poseAction == null)
                return -1;

            var action = pose.poseAction[pose.inputSource];

            if (action == null)
                return -1;

            return (int)action.trackedDeviceIndex;
        }
    }
}
