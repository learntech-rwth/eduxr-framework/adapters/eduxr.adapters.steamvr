﻿using UnityEngine;

using Valve.VR;
using Valve.VR.InteractionSystem;

namespace OmiLAXR.Adapters.SteamVR
{
    public class ControllerHints : MonoBehaviour
    {
        public string teleportHintText = "Teleport";
        public string interactUIHintText = "InteractUI";

        public SteamVR_Action_Boolean teleportAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Teleport");
        public SteamVR_Action_Boolean interactUIAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("InteractUI");
        public void ShowControllerTriggerHint() => ShowControllerHint(interactUIAction, interactUIHintText);
        public void ShowControllerTeleportHint() => ShowControllerHint(teleportAction, teleportHintText);

        public void HideAllControllerHints()
        {
            var player = Player.instance;
            FindObjectOfType<Teleport>().CancelTeleportHint();
            ControllerButtonHints.HideAllButtonHints(player.leftHand);
            ControllerButtonHints.HideAllButtonHints(player.rightHand);
            ControllerButtonHints.HideAllTextHints(player.leftHand);
            ControllerButtonHints.HideAllTextHints(player.rightHand);
            player.leftHand.enabled = player.rightHand.enabled = false;
        }

        public void ShowControllerHint(string actionName, string text)
            => ShowControllerHint(SteamVR_Input.GetAction<SteamVR_Action_Boolean>(actionName), text);

        public void ShowControllerHint(SteamVR_Action_Boolean action, string text)
        {
            if (!Learner.Instance.IsVR)
                return;

            var hand = Learner.Instance.GetPrimaryHand();
            var svrHand = hand.GameObject.GetComponent<Hand>();
            
            HideAllControllerHints();
            
            ControllerButtonHints.ShowButtonHint(svrHand, action);
            ControllerButtonHints.ShowTextHint(svrHand, action, text, true);
        }


    }
}