﻿using OmiLAXR.xAPI.Tracking;
using OmiLAXR.xAPI.Tracking.Teleport;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace OmiLAXR.Adapters.SteamVR
{
    [AddComponentMenu("OmiLAXR / Interaction / Teleport System Adapter")]
    public class TeleportSystemAdapter : MonoBehaviour, ITeleportAdapter
    {
        public SteamVR_Action_Boolean teleportAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Teleport");
        
        private void Start()
        {
            // Add normal teleport event listener
            Teleport.Player.Listen(HandleTeleportEvent);
            // Add change scene listener
            Teleport.ChangeScene.Listen(ChangeScene);

            if (MainTrackingSystem.Instance)
            {
                var system = MainTrackingSystem.Instance.GetSubSystem<TeleportTrackingSystem>();
                system.ApplyAdapter(this);
            }
        }

        private void ChangeScene(float fadeTime)
        {
            OnTeleport?.Invoke(new TeleportEventArgs(null, TeleportType.ChangeScene, fadeTime));   
        }

        private void OnDestroy()
        {
            Teleport.Player.Remove(HandleTeleportEvent);
            Teleport.ChangeScene.Remove(ChangeScene);
        }
        
        private void HandleTeleportEvent(TeleportMarkerBase marker)
        {
            var teleportArea = marker.GetComponent<TeleportArea>();
            if (teleportArea)
            {
                OnTeleport?.Invoke(new TeleportEventArgs(marker.gameObject, TeleportType.InArea));
                return;
            }
            
            OnTeleport?.Invoke(new TeleportEventArgs(marker.gameObject, TeleportType.ToPoint));
        }

        public string GetActionName() => teleportAction.ToString();

        public event TeleportEventHandler OnTeleport;
    }
}