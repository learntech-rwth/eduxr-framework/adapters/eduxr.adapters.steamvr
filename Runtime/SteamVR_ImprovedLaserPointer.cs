﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using OmiLAXR.Interaction;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;
using LaserPointerEventArgs = OmiLAXR.Interaction.LaserPointerEventArgs;


namespace OmiLAXR.Adapters.SteamVR
{
    /// <summary>
    /// Optimized SteamVR_LaserPointer Class
    /// </summary>
    [AddComponentMenu("OmiLAXR / Interaction / SteamVR Laser Pointer (OmiLAXR Adapter)")]
    public class SteamVR_ImprovedLaserPointer : SteamVR_LaserPointer, ILaserPointer
    {
        private bool _isActive = true;
        private Transform _previousContact = null;

        public new event LaserPointerEventHandler PointerClick;
        public new event LaserPointerEventHandler PointerIn;
        public new event LaserPointerEventHandler PointerOut;
        public event LaserPointerEventHandler PointerHit;
        public event LaserPointerEventHandler OnEnter;
        public event LaserPointerEventHandler OnLeave;
        public event LaserPointerEventHandler PointerBeginHold;
        public event LaserPointerEventHandler PointerEndHold;
        public event LaserPointerEventHandler PointerHolding;

        public event Action<RaycastHit> OnEnterSomething;
        public event Action OnLeaveSomething;
        public Ray GetRay() => ray;

        public event Action<RaycastHit> OnHit;

        public bool showsAlways = true;

        public readonly List<GameObject> limitedOn = new List<GameObject>();

        private bool wasHit = false;
        public Color hoverColor = new Color(0.0156f, 0.78f, 1.0f, 1.0f);

        private Pointable selectedTarget = null;

        public Vector3 direction => transform.forward;

        public Ray ray => new Ray(transform.position, transform.forward);

        private GameObject hitPointObject;
        private MeshRenderer hitPointMeshRenderer;

        [Tooltip("Collection of actions, when on which the laser gets invisible")]
        public SteamVR_Action_Boolean[] hideOnLaserActions = new[]
        {
            SteamVR_Input.GetBooleanAction("Teleport")
        };

        private bool loadedController = false;
        private static readonly int Color1 = Shader.PropertyToID("_Color");

        private void Awake()
        {
            CreateHitPoint();
            SteamVR_Events.RenderModelLoaded.AddListener(OnRenderModelLoaded);
        }

        private void OnRenderModelLoaded(SteamVR_RenderModel model, bool state)
        {
            loadedController = (int)model.index == pose.GetDeviceIndex();
        }

        private void CreateHitPoint()
        {
            Destroy(hitPointObject);
            hitPointObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Destroy(hitPointObject.GetComponent<SphereCollider>());
            hitPointObject.transform.SetParent(transform);
            hitPointObject.transform.localScale = Vector3.one * 0.01f;
            hitPointObject.name = "HitPoint";
            hitPointObject.SetActive(false);

            var newMaterial = new Material(Shader.Find("Unlit/Color"));
            newMaterial.SetColor(Color1, color);

            hitPointMeshRenderer = hitPointObject.GetComponent<MeshRenderer>();
            hitPointMeshRenderer.material = newMaterial;
        }

        /// <summary>
        /// Needed to be overwritten completely
        /// </summary>
        private void Update()
        {
            if (!_isActive)
                return;

            if (!loadedController)
            {
                HideLaser();
                return;
            }

            // Dont do anything if it is hidden anyway by specified inputs
            if (IsHiddenByInputs())
                return;

            // Hide laser and show him if he shall be shown always
            HideLaser();

            if (showsAlways)
                ShowLaser();

            // calculate raycast contact point
            var dist = 100f;
            var isHit = Physics.Raycast(ray, out var hit);

            var isReallyHit = isHit;
            if (isHit && gameObject.CompareTag("IgnorePointer"))
                isReallyHit = false;

            // Check if only allowed game objects are hit
            if (isReallyHit && limitedOn.Count > 0 && !limitedOn.Contains(hit.transform.gameObject))
                return;

            HandleHitEvents(isReallyHit, hit);

            CheckPressingState(hit);

            if (_previousContact && _previousContact != hit.transform)
            {
                var args = new Interaction.LaserPointerEventArgs(this, _previousContact, 0f, hit);
                PointerOut?.Invoke(args);
                _previousContact = null;
            }

            if (isHit && _previousContact != hit.transform)
            {
                var args = new Interaction.LaserPointerEventArgs(this, hit.transform, hit.distance, hit);
                PointerIn?.Invoke(args);
                _previousContact = hit.transform;
            }

            if (!isHit)
            {
                _previousContact = null;
            }

            if (isHit && hit.distance < 100f)
            {
                dist = hit.distance;
            }

            if (isHit && interactWithUI.GetStateUp(pose.inputSource))
            {
                var argsClick = new Interaction.LaserPointerEventArgs(this, hit.transform, hit.distance, hit);
                PointerClick?.Invoke(argsClick);
            }

            if (isHit)
            {
                OnHit?.Invoke(hit);
            }

            HandleLaserVisibility(isHit, hit);
            HandleLaserStyle(isHit, hit, dist);

            pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);

            // Handle hit point position
            if (isHit && hitPointObject)
                hitPointObject.transform.position = hit.point;
            else
                hitPointObject?.SetActive(false);
        }

        private void HandleHitEvents(bool isHit, RaycastHit hit)
        {
            if (!wasHit && isHit)
            {
                OnEnterSomething?.Invoke(hit);
                wasHit = true;
            }
            else if (wasHit && !isHit)
            {
                OnLeaveSomething?.Invoke();
                wasHit = false;
            }
        }

        private bool IsHiddenByInputs()
        {
            if (!hideOnLaserActions.Any(actions => actions.GetState(pose.inputSource)))
                return false;
            HideLaser();
            return true;
        }

        private void HideHitPoint() => hitPointObject?.SetActive(false);
        public Color GetLaserColor() => pointer.GetComponent<MeshRenderer>().material.color;

        private void HandleLaserStyle(bool isHit, RaycastHit hit, float dist)
        {
            var mr = pointer.GetComponent<MeshRenderer>();
            if (interactWithUI != null && interactWithUI.GetState(pose.inputSource))
            {
                pointer.transform.localScale = new Vector3(thickness * 1.5f, thickness * 1.5f, dist);
                mr.material.color = clickColor;
                hitPointMeshRenderer.material.color = clickColor;
            }
            else
            {
                pointer.transform.localScale = new Vector3(thickness, thickness, dist);
                mr.material.color = IsValidHit(isHit, hit) ? Color.red : color;
                hitPointMeshRenderer.material.color = mr.material.color;
            }
        }

        private void HandleLaserVisibility(bool isHit, RaycastHit hit)
        {
            if (!IsValidHit(isHit, hit))
                return;

            if (showsAlways || isHit)
                ShowLaser();
            else
                HideLaser();
        }

        private static bool IsValidHit(bool isHit, RaycastHit hit)
        {
            return isHit && !(hit.collider && hit.collider.gameObject &&
                              hit.collider.gameObject.CompareTag("IgnorePointer"));
        }

        private void CheckPressingState(RaycastHit hit)
        {
            var action = interactWithUI;
            var inputSource = pose.inputSource;

            if (action == null)
                return;

            var pointerArgsIn = new Interaction.LaserPointerEventArgs(this, hit.transform, hit.distance, hit);

            if (action.GetState(inputSource)) // is trigger down
            {
                if (!action.GetLastState(inputSource)) // but was not down before
                {
                    selectedTarget = TransformToPointable(hit.transform);
                    selectedTarget?.onPressed.Invoke(pointerArgsIn);
                }

                selectedTarget?.onIsHolding.Invoke(pointerArgsIn);
            }
            else if (action.GetLastState(inputSource))
            {
                selectedTarget?.onReleased.Invoke(pointerArgsIn);
                selectedTarget = null;
            }
        }

        public static Pointable TransformToPointable(Transform transform)
        {
            if (transform == null || transform.gameObject == null)
                return null;
            return transform.gameObject.GetComponent<Pointable>();
        }

        /// <summary>
        /// Just hide the laser.
        /// </summary>
        private void HideLaser()
        {
            if (pointer)
                pointer.SetActive(false);
            if (hitPointObject)
                hitPointObject.SetActive(false);
        }

        /// <summary>
        /// Just show the laser.
        /// </summary>
        private void ShowLaser()
        {
            if (pointer)
                pointer.SetActive(true);
            if (hitPointObject)
                hitPointObject.SetActive(true);
        }

        public bool ProjectOnPlane(Vector3 u, Vector3 v, Vector3 planePosition, out Vector3 touchPoint)
            => ProjectOnPlane(planeNormal: Vector3.Cross(u, v).normalized, planePosition, out touchPoint);

        public bool ProjectOnPlane(Vector3 planeNormal, Vector3 planePosition, out Vector3 touchPoint)
        {
            var plane = new Plane(planeNormal, planePosition);
            return ProjectOnPlane(plane, out touchPoint);
        }

        public bool ProjectOnPlane(Plane plane, out Vector3 touchPoint)
        {
            // Reset
            touchPoint = Vector3.zero;

            // Raycast laser pointer on planes
            if (!plane.Raycast(ray, out var d))
                return false;

            // Get Touch Point on Plane
            touchPoint = ray.GetPoint(d);
            return true;
        }

        public GameObject GetGameObject() => gameObject;

        public override void OnPointerIn(PointerEventArgs e)
        {
            throw new Exception("This event is overwritten and invalid. Use TriggerPointerIn() instead.");
        }

        public override void OnPointerClick(PointerEventArgs e)
        {
            throw new Exception("This event is overwritten and invalid. Use TriggerPointerClick() instead.");
        }

        public override void OnPointerOut(PointerEventArgs e)
        {
            throw new Exception("This event is overwritten and invalid. Use TriggerPointerOut() instead.");
        }
    }
}