using System.Collections;

using OmiLAXR.Extensions;
using OmiLAXR.Interaction;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;

namespace OmiLAXR.Adapters.SteamVR
{
    [RequireComponent(typeof(TeleportSystemAdapter))]
    [AddComponentMenu("GameObject / OmiLAXR / SteamVR Adapter")]
    [DefaultExecutionOrder(-1000)]
    public class SteamVR_Adapter : XR_Framework_Adapter
    {
        private static SteamVR_Adapter _instance;
        /// <summary>
        /// Singleton instance of the SteamVR_Adapter. Only one can exist at a time.
        /// </summary>
        public static SteamVR_Adapter Instance
            => _instance ??= FindObjectOfType<SteamVR_Adapter>();
        public override MonoBehaviour GetInstance() => Instance;

        private Player _player;
        private static bool CanVR => Valve.VR.SteamVR.instance != null;

        protected override bool IsVR() => CanVR;

        public override bool IsLoading() =>
            Valve.VR.SteamVR.initializedState == Valve.VR.SteamVR.InitializedStates.None ||
            Valve.VR.SteamVR.initializedState == Valve.VR.SteamVR.InitializedStates.Initializing;

        protected override void Awake()
        {
            base.Awake();
        }

        private static void ClearSingletonSteamVR(Scene scene)
        {
            scene.DestroyChildsImmediate<Player>();
        }

        private IEnumerator Start()
        {
            while (IsLoading())
                yield return null;
            LoadPlayer();
        }

        private GameObject GetPlayer()
        {
            // get player from scene if exists
            if (Player.instance)
            {
                _player = Player.instance;
                return playerReference = _player.gameObject;
            }

            // else create one
            if (!playerPrefab)
                DebugLog.OmiLAXR.Error("No player prefab provided", this);

            var playerGo = playerReference = Instantiate(playerPrefab);
            playerGo.name = "Player";

            return playerGo;
        }

        protected override GameObject LoadPlayer()
        {
            if (_player)
                return _player.gameObject;

            var playerGo = GetPlayer();
            _player = playerGo.GetComponent<Player>();
            
            if (!CanVR)
            {
                Learner.AssignPlayer(new NonVRPlayer(_player));
                return playerGo;
            }

            Learner.AssignPlayer(new VRPlayer(_player));
            LaserPointerHandler.Instance.Init(Learner.Player);

            return playerGo;
        }
    }
}