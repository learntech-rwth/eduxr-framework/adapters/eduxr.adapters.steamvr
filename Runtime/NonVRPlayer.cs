﻿using OmiLAXR.Extensions;
using OmiLAXR.Interaction;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace OmiLAXR.Adapters.SteamVR
{
    /// <summary>
    /// Implementing a player which is not using VR for SteamVR by using NoSteamVRFallbackObjects.
    /// </summary>
    public class NonVRPlayer : IPlayer
    {
        private readonly GameObject _main;
        private readonly GameObject _hand;
        private readonly Transform _body;
        private readonly Transform _head;
        private readonly Camera _camera;
        private readonly NonVRPointer _pointer;

        private readonly string[] _controllerActions = new[] { "Mouse", "Keyboard", "GamePad" };

        public NonVRPlayer(Player player)
        {
            var playerGameObject = player.gameObject;
            
            _main = playerGameObject.Find("NoSteamVRFallbackObjects", true);
            var objects = _main.Find("FallbackObjects", true);
            
            _hand = _main.Find("FallbackHand", true);
            _head = objects.transform.Find("FollowHead");

            if (!_head)
            {
                _head = GameObject.Find("FollowHead")?.transform;
            }
            
            _pointer = _hand.Find("Indicator", true).AddComponent<NonVRPointer>();
            _body = objects.transform;
            _camera = Camera.main;
        }

        public Vector3 WorldPosition => GetTransform().position;
        public Vector3 WorldRotation => GetTransform().eulerAngles + _camera.transform.eulerAngles;

        /// <summary>
        /// Representing the transform of the player object.
        /// </summary>
        /// <returns>Transform of 'NoSteamVRFallbackObjects' of SteamVR Player Prefab.</returns>
        public Transform GetTransform() => _main.transform;
        /// <summary>
        /// Representing the head of the player object.
        /// </summary>
        /// <returns>Transform of 'FollowHead' object of SteamVR Player Prefab.</returns>
        public Transform GetHead() => _head;
        /// <summary>
        /// Representing the body of the player object.
        /// </summary>
        /// <returns>Transform of ''FallbackObjects' object of SteamVR Player prefab.</returns>
        public Transform GetBody() => _body;
        /// <summary>
        /// Representing the laser pointer representation of a non VR player.
        /// </summary>
        /// <returns>Reference to 'Indicator' object of SteamVR Player prefab.</returns>
        public ILaserPointer GetLeftPointer() => _pointer;
        /// <summary>
        /// Representing the laser pointer representation of a non VR player.
        /// </summary>
        /// <returns>Reference to 'Indicator' object of SteamVR Player prefab.</returns>
        public ILaserPointer GetRightPointer() => _pointer;
        /// <summary>
        /// Representing the left hand of a non VR Player.
        /// </summary>
        /// <returns>Reference to 'FallbackHand' object of SteamVR Player prefab.</returns>
        public GameObject GetLeftHand() => _hand;
        /// <summary>
        /// Representing the left hand of a non VR Player.
        /// </summary>
        /// <returns>Reference to 'FallbackHand' object of SteamVR Player prefab.</returns>
        public GameObject GetRightHand() => _hand;
        /// <summary>
        /// Representing the camera of a non VR Player.
        /// </summary>
        /// <returns>Reference to the 'Camera' component of the 'FallbackObjects' object of SteamVR Player prefab.</returns>
        public Camera GetCamera() => _camera;

        public XRType XRType => XRType.Desktop;

        /// <summary>
        /// Representing a list of possible controller actions the player can do.
        /// </summary>
        /// <returns>Returns a list of controller actions.</returns>
        public string[] GetControllerActions() => _controllerActions;
        public event ActionChangedHandler OnActionChanged;
        public const float Height = 1.75f;
    }
}