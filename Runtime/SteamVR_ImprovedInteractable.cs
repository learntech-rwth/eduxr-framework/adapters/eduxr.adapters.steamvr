﻿using UnityEngine;
using Valve.VR.InteractionSystem;

namespace OmiLAXR.Adapters.SteamVR
{
    [DisallowMultipleComponent]
    [AddComponentMenu("OmiLAXR / Interaction / SteamVR Interactable (OmiLAXR compatible)")]
    public class SteamVR_ImprovedInteractable : Interactable, Interaction.IInteractable
    {
        public void SetHighlightOnHover(bool highlightValue)
        {
            highlightOnHover = highlightValue;
        }

        public bool IsHovering() => isHovering;
    }
}