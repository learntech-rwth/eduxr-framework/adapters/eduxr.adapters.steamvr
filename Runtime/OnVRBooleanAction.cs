﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

namespace OmiLAXR.Adapters.SteamVR
{

    /// <summary>
    /// Simple script that globally listens to the vr controllers trigger and fires
    /// events if it is pressed.
    /// </summary>

    public class OnVRBooleanAction : MonoBehaviour
    {
        [Tooltip("Action to listen to")]
        public SteamVR_Action_Boolean action;

        [Tooltip("Sources to interact with")]
        public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;

        public UnityEvent OnPress = new UnityEvent(), OnRelease = new UnityEvent();

        public bool IsHeld { get; private set; } = false;

        private void OnEnable()
        {
            if (action != null)
            {
                action.AddOnChangeListener(Press, inputSource);
            }
        }

        private void OnDisable()
        {
            if (action != null)
            {
                action.RemoveOnChangeListener(Press, inputSource);
            }
        }

        private void Press(SteamVR_Action_Boolean actionIn, SteamVR_Input_Sources source, bool newState)
        {
            IsHeld = newState;

            if (newState)
            {
                OnPress.Invoke();
            }
            else
            {
                OnRelease.Invoke();
            }
        }
    }
}