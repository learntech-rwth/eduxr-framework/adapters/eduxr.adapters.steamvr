﻿using System;
using OmiLAXR.Interaction;
using UnityEngine;

namespace OmiLAXR.Interaction
{

    /// <summary>
    /// Allows interaction with a rectangular area.
    /// </summary>

    [RequireComponent(typeof(Pointable))]
    public class AreaInteraction : MonoBehaviour
    {
        private RectTransform rect;
        private Pointable pointable;
        private bool isHeld = false;

        private Vector3 hit;

        public event Action<Vector2> OnSelect;

        private void Start()
        {
            pointable = GetComponent<Pointable>();
            pointable.onSelect.AddListener(_ => isHeld = true);
            pointable.onUnselect.AddListener(_ => isHeld = false);

            rect = GetComponent<RectTransform>();

            LaserPointerHandler.Instance.OnHit += hitPoint => hit = hitPoint.point;
        }

        private void Update()
        {
            if (isHeld)
            {
                // normalize into [0, 1]^2 space (unclamped)
                var local = rect.worldToLocalMatrix.MultiplyPoint(hit);

                var point = new Vector2(
                    local.x / rect.rect.width + rect.pivot.x,
                    local.y / rect.rect.height + rect.pivot.y);

                OnSelect?.Invoke(point);
            }
        }
    }
}