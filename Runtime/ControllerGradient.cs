﻿using UnityEngine;
using Valve.VR;

namespace OmiLAXR.Adapters.SteamVR
{
    [RequireComponent(typeof(ColorPicker))]
    public class ControllerGradient : MonoBehaviour
    {
        private ColorPicker _colorPicker;

        private void Awake()
        {
            SteamVR_Events.RenderModelLoaded.AddListener(RenderModelLoaded);
        }

        // Start is called before the first frame update
        private void Start()
        {
            // Get color picker
            _colorPicker = GetComponent<ColorPicker>();
        }

        private void RenderModelLoaded(SteamVR_RenderModel renderModel, bool success)
        {
            if (!success)
                return;

            var placeHand = Learner.Instance.GetPrimaryHand().GameObject;
            var rm = placeHand.GetComponentInChildren<SteamVR_RenderModel>();

            if (rm.index != renderModel.index)
                return;

            // init body mesh
            rm.AddControllerBodyCollider();
        }

        public void PlaceOnController(GameObject controllerGameObject)
        {
            var rm = controllerGameObject.GetComponentInChildren<SteamVR_RenderModel>();
            var gradient = _colorPicker.gradientObject;
            gradient.SetActive(false);
            rm.PlaceOnTrackpad(gradient);
            gradient.transform.localPosition = new Vector3(0, 0, 0.003f);
            gradient.transform.localEulerAngles = new Vector3(0, -180, 90);
            gradient.transform.localScale = new Vector3(0.016f, 0.016f, 1);
            gradient.SetActive(true);
        }
    }

}